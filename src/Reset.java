import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Reset {
	public static void main() {
//		Reset serializerItem = new Reset();
//		serializerItem.refill();
	}
	public void refill() {
		foodInfo snickers = new foodInfo("Snickers", 1, 215, 20, 100);
		foodInfo hotCheetos = new foodInfo("Hot Cheetos", 1, 160, 20, 99);
		foodInfo sunFlowerSeeds = new foodInfo("Sunflower Seeds", 1, 180, 20, 59);
		foodInfo mms = new foodInfo("M&Ms", 1, 240, 20, 150);
		foodInfo butterFinger = new foodInfo("Butterfinger", 1, 275, 20,175);
		foodInfo milkyWay = new foodInfo("MilkyWay", 1, 264, 20, 175);
		foodInfo twix = new foodInfo("Twix", 1, 250, 20, 89);
		foodInfo laysChips = new foodInfo("Lays Chips", 1, 160, 20, 100);
		foodInfo honeyBun = new foodInfo("Honey Bun", 1, 440, 20, 150);
		foodInfo pretzel = new foodInfo("Pretzel", 1, 108, 20, 200);

		foodInfo coke = new foodInfo("Coke", 2, 140, 20, 150);
		foodInfo pepsi = new foodInfo("Pepsi", 2, 250, 20, 150);
		foodInfo sprite = new foodInfo("Sprite", 2, 192, 20, 150);
		foodInfo mountainDew = new foodInfo("Mountain Dew", 2, 170, 20,150);
		foodInfo water = new foodInfo("Water", 2, 0, 20, 99);
		foodInfo dietCoke = new foodInfo("Diet Coke", 2, 4, 20, 150);
		foodInfo dietPepsi = new foodInfo("Diet Pepsi", 2, 0, 20, 150);
		foodInfo redBull = new foodInfo("Red Bull", 2, 110, 20, 200);
		foodInfo gatorade = new foodInfo("Gatorade", 2, 50, 20, 169);
		foodInfo fanta = new foodInfo("Fanta", 2, 120, 20, 150);

		foodInfo[] vendingArray = { snickers, hotCheetos, sunFlowerSeeds, mms,
				butterFinger, milkyWay, twix, laysChips, honeyBun, pretzel,
				coke, pepsi, sprite, mountainDew, water, dietCoke, dietPepsi,
				redBull, gatorade, fanta };
		try {
			FileOutputStream fileOutput = new FileOutputStream("address.ser");
			ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
			objectOutput.writeObject(vendingArray);
			objectOutput.close();
			System.out.println("Refilled");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}