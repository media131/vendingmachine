import java.io.IOException;
import java.util.Scanner;

public class vendingDriverClass {
	private static foodInfo[] tempArray;

	public static void main(String[] args) throws IOException {		
		Scanner keyboard = new Scanner(System.in);
		
		Deserializer deserializer = new Deserializer(); //creates deserializer to read files
		tempArray=deserializer.deserialzeAddress(); //reads the default file = 20 stock and makes an array
		System.out.println("Press enter to continue");
		keyboard.nextLine();		
		vendingMachine.main(tempArray); //uses the array for calculations/random customers and displays
		
		System.out.println("");
		System.out.println("Enter y to refill all stock to 20.");
		System.out.println("Enter anything else to keep stock.");
		String answer = keyboard.nextLine();
		if (answer.contains("y"))
		{
			Reset.main();			
		}
		System.out.println("Done");	
	}
}































