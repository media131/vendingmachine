import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class vendingMachine {
	private static Random rand = new Random(); //random item
	private static int totalProfit1=0,totalProfit2=0;
	private static int[] totalSold={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	public static void main(foodInfo[] tempArray) throws IOException  {
		
		
		int randNum = rand.nextInt(101), randomCustomers = 200 + randNum;//random number to generate between 200-300
		int timeInterval = 1020 / randomCustomers, customerTimeInterval = 300, minutes;//determines random time intervals and starting time = 5:00 AM
		int randCustMoney,qrt,dim,nik,pen,amount;
		
		Date date = new Date();//date item for computer date
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//date saved at the exact same time it closes
		File file = new File("Recorded-Logs-"+dateFormat.format(date) + ".txt");//filename is the date + .txt
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		StringBuilder sbuilder = new StringBuilder();//item/items to be written to file
				
		
		File file2 = new File("Snack-Machine-"+dateFormat.format(date) + ".txt");
		File file3 = new File("Drink-Machine-"+dateFormat.format(date) + ".txt");
		
		BufferedWriter sec = new BufferedWriter(new FileWriter(file2));
		StringBuilder secBuilder = new StringBuilder();
		BufferedWriter sec2 = new BufferedWriter(new FileWriter(file3));
		StringBuilder secBuilder2 = new StringBuilder();

		System.out.println("\n" + "Number of Customers: " + randomCustomers);
		
		for (int i = 1; i <= randomCustomers; ++i) {
			System.out.println();			
			// 300 Minutes = 5AM
			// 1200 Minutes = 10PM
			customerTimeInterval = customerTimeInterval + timeInterval;

			int hours = customerTimeInterval / 60;//for hour
			minutes = customerTimeInterval % 60;//for use for minutes
			

			if (minutes < 10) { //Displays the proper time for single/double digit minutes and sets PM/AM for noon properly
				if (customerTimeInterval < 780) {
					if (customerTimeInterval > 720) {
						System.out.println("Time: " + hours + ":0" + minutes + " PM");
						sbuilder.append("Time: " + hours + ":0" + minutes + " PM");
					} else {
						System.out.println("Time: " + hours + ":0" + minutes + " AM");
						sbuilder.append("Time: " + hours + ":0" + minutes 	+ " AM");
					}
				} else {
					System.out.println("Time: " + (hours - 12) + ":0" + minutes + " PM");
					sbuilder.append("Time: " + (hours - 12) + ":0" + minutes + " PM");
				}
			} else if (customerTimeInterval < 780) {
				if (customerTimeInterval > 720) {
					System.out.println("Time: " + hours + ":" + minutes + " PM");
					sbuilder.append("Time: " + hours + ":" + minutes + " PM");
				} else {
					System.out.println("Time: " + hours + ":" + minutes + " AM");
					sbuilder.append("Time: " + hours + ":" + minutes + " AM");
				}
			} else {
				System.out.println("Time: " + (hours - 12) + ":" + minutes + " PM");
				sbuilder.append("Time: " + (hours - 12) + ":" + minutes + " PM");
			}
			
			int randItem = rand.nextInt(20); // customer chooses random item
			randCustMoney = rand.nextInt(250)+50; //random number of customers
			
			int change = randCustMoney-tempArray[randItem].getPrice(); //determines if there is enough money and displays change
			
			System.out.println("Customer deposited: " + intToMoney(randCustMoney));
			System.out.println("Customer selected: " + tempArray[randItem].getName()+"["+intToMoney(tempArray[randItem].getPrice())+"]"+" ["+(tempArray[randItem]).getStockNumber()+" left]");
			sbuilder.append(System.lineSeparator());			
			sbuilder.append("Customer deposited: " + intToMoney(randCustMoney));
			sbuilder.append(System.lineSeparator());
			sbuilder.append("Customer selected: " + tempArray[randItem].getName()+"["+intToMoney(tempArray[randItem].getPrice())+"]"+" ["+(tempArray[randItem]).getStockNumber()+" left]");
			
						
			if (change <0 && !(tempArray[randItem].getStockNumber()==0))	{				
				amount=randCustMoney;
				qrt = amount / 25;
		    	amount = amount % 25;
		    	dim = amount / 10;
		    	amount = amount % 10;
		    	nik = amount / 5;
		    	amount = amount % 5;
		    	pen = amount / 1;
		    	amount = amount % 1;		    	
				System.out.println("Change: "+intToMoney(randCustMoney));
		    	System.out.println("Customer [" + i + "] does not have enough money");
		    	System.out.println(qrt + " quarters");    	
		    	System.out.println(dim + " dimes");
		    	System.out.println(nik + " nickels");
		    	System.out.println(pen + " pennies");		    	
		    	sbuilder.append(System.lineSeparator());
				sbuilder.append("Change: "+intToMoney(randCustMoney));
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(qrt + " quarters");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(dim + " dimes");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(nik + " nickels");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(pen + " pennies");				
				sbuilder.append(System.lineSeparator());
				sbuilder.append("Customer [" + i + "] does not have enough money");
				sbuilder.append(System.lineSeparator());
				sbuilder.append(System.lineSeparator());
			}	else if (tempArray[randItem].getStockNumber()==0)	{
				amount=randCustMoney;
				qrt = amount / 25;
		    	amount = amount % 25;
		    	dim = amount / 10;
		    	amount = amount % 10;
		    	nik = amount / 5;
		    	amount = amount % 5;
		    	pen = amount / 1;
		    	amount = amount % 1;	    
		    			    	
				System.out.println("Change: "+intToMoney(randCustMoney));							
		    	System.out.println(qrt + " quarters");    	
		    	System.out.println(dim + " dimes");
		    	System.out.println(nik + " nickels");
		    	System.out.println(pen + " pennies");			    	
		    	System.out.println("Item Number"+ "["+randItem+"]: " + (tempArray[randItem]).getName()+" is out of stock");
				sbuilder.append(System.lineSeparator());
				sbuilder.append("Change: "+intToMoney(randCustMoney));
				sbuilder.append(System.lineSeparator());
		    	sbuilder.append(qrt + " quarters");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(dim + " dimes");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(nik + " nickels");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(pen + " pennies");
				sbuilder.append(System.lineSeparator());
				sbuilder.append("Item Number"+ "["+randItem+"]: " + (tempArray[randItem]).getName()+" is out of stock");
				sbuilder.append(System.lineSeparator());
				sbuilder.append(System.lineSeparator());
			}	else	{
				tempArray[randItem].setStockNumber((tempArray[randItem].getStockNumber()-1));					
				amount=change;
				qrt = amount / 25;
		    	amount = amount % 25;
		    	dim = amount / 10;
		    	amount = amount % 10;
		    	nik = amount / 5;
		    	amount = amount % 5;
		    	pen = amount / 1;
		    	amount = amount % 1;		    	
				System.out.println("Change: "+intToMoney(change));
		    	System.out.println(qrt + " quarters");    	
		    	System.out.println(dim + " dimes");
		    	System.out.println(nik + " nickels");
		    	System.out.println(pen + " pennies");
				System.out.println("Customer [" + i + "] has bought Item Number"+ "["+randItem+"]: " + (tempArray[randItem]).getName()+" ["+(tempArray[randItem]).getStockNumber()+" left]");
				sbuilder.append(System.lineSeparator());
				sbuilder.append("Change: "+intToMoney(change));
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(qrt + " quarters");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(dim + " dimes");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(nik + " nickels");
		    	sbuilder.append(System.lineSeparator());
		    	sbuilder.append(pen + " pennies");		    	
				sbuilder.append(System.lineSeparator());
				sbuilder.append("Customer [" + i + "] has bought Item Number"+ "["+randItem+"]: " + (tempArray[randItem]).getName()+" ["+(tempArray[randItem]).getStockNumber()+" left]");
				sbuilder.append(System.lineSeparator());
				sbuilder.append(System.lineSeparator());			
				
				if (tempArray[randItem].getDescription()==1)	{
					totalSold[randItem]++;
					totalProfit1=totalProfit1+tempArray[randItem].getPrice();					
				}	else	{
					totalSold[randItem]++;
					totalProfit2=totalProfit2+tempArray[randItem].getPrice();					
				}				
			}			
		}
		System.out.println("");
		for (int i=0;i<10;i++)	{	
			System.out.println(tempArray[i].getName()+" sold "+totalSold[i]+" units.");
			secBuilder.append(System.lineSeparator());
			secBuilder.append(tempArray[i].getName()+" sold "+totalSold[i]+" units.");			
		}	
		for (int i=10;i<20;i++)	{	
			System.out.println(tempArray[i].getName()+" sold "+totalSold[i]+" units.");
			secBuilder2.append(System.lineSeparator());
			secBuilder2.append(tempArray[i].getName()+" sold "+totalSold[i]+" units.");			
		}
		System.out.println("");
		System.out.println("Snack Machine's daily profit is "+intToMoney(totalProfit1));
		secBuilder.append(System.lineSeparator());
		secBuilder.append("Snack Machine's daily profit is "+intToMoney(totalProfit1));
		System.out.println("");
		System.out.println("Drink Machine's daily profit is "+intToMoney(totalProfit2));
		secBuilder2.append(System.lineSeparator());
		secBuilder2.append("Drink Machine's daily profit is "+intToMoney(totalProfit2));
		System.out.println("");
		System.out.println("Total daily profit is "+intToMoney(totalProfit1+totalProfit2));
		sbuilder.append(System.lineSeparator());
		sbuilder.append("Total daily profit is "+intToMoney(totalProfit1+totalProfit2));
		secBuilder.append(System.lineSeparator());
		secBuilder.append("Total daily profit is "+intToMoney(totalProfit1+totalProfit2));
		secBuilder2.append(System.lineSeparator());
		secBuilder2.append("Total daily profit is "+intToMoney(totalProfit1+totalProfit2));		

		out.append(sbuilder);//adds info to file
		out.close();//closes file object
		sec.append(secBuilder);//adds info to file
		sec.close();//closes file object
		sec2.append(secBuilder2);//adds info to file
		sec2.close();//closes file object		
		Serializer(tempArray);//saves the array info to file
	}
	
	private static void Serializer(foodInfo[] tempArray) {
		try {
			FileOutputStream fileOutput = new FileOutputStream("address.ser");
			ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
			objectOutput.writeObject(tempArray);
			objectOutput.close();
		} catch (Exception ex)	{
			ex.printStackTrace();	
		}		
	}

	private static String intToMoney(int x) {		
		String stringPrice="";
		stringPrice="$"+(x/100)+"."+((x%100)/10)+""+((x%100)%10);		
		return stringPrice;
	}	
}
















































