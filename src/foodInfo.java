import java.io.Serializable;
public class foodInfo implements Serializable{
	String name;
	int description,calories,stockNumber,price;	
	public foodInfo()	{
		name="";
		description=0;
		calories=0;
		price=0;
		stockNumber=0;		
	}	
	public foodInfo(String name, int description, int calories, int stockNumber, int price)	{
		super();
		this.name=name;
		this.description=description;
		this.calories=calories;
		this.price=price;
		this.stockNumber=stockNumber;		
	}	
	public String getName() 
	{		return name;	
	}
	public void setName(String name) 
	{		this.name = name;
	}
	public int getDescription() 
	{		return description;
	}
	public void setDescription(int description) 
	{		this.description = description;
	}
	public int getCalories() 
	{		return calories;
	}
	public void setCalories(int calories) 
	{		this.calories = calories;
	}
	public int getStockNumber() 
	{		return stockNumber;
	}
	public void setStockNumber(int stockNumber)
	{		this.stockNumber = stockNumber;
	}
	public int getPrice() 
	{		return price;
	}
	public void setPrice(int price)
	{		this.price = price;
	}

	public String toString() {
		return 	name
				//+ "\nDescription: "+ description 
				+ "\n" + calories+" Calories" 
				+ "\nNumber of Stock: "	+ stockNumber 
				+ "\nPrice: " + "$"+(price/100)+"."+((price%100)/10)+""+((price%100)%10)
				+ "\n";
	}

}
























